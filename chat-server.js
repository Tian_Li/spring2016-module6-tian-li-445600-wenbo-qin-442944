// Require the packages we will use:
var http = require("http"),
socketio = require("socket.io"),
fs = require("fs");

// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.

	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.

		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
	


});
app.listen(3456);

var users = {};
var rooms = {};
var private_rooms = [];
var userInRoom = [];
//userInRoom['lobby']="";
var blacklist = [];

var owner = [];



// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.

	//接收用户登录
	socket.on('login', function(data){
		socket.user = data;

		users[data]=socket.id;
		socket.room = 'lobby';
		rooms['lobby'] = socket.room;

		socket.join('lobby');
		console.log("a new user logged in: "+data);
		console.log("data 类型: "+Object.prototype.toString.apply(data));
		io.sockets.to(socket.room).emit("logged_in",data);
		io.sockets.to(socket.room).emit("current_users", users);
		
		
		
		if(userInRoom['lobby']==undefined){
			var userlist = [];
			userlist.push(data);
			userInRoom['lobby']=userlist;
		}else{
			userInRoom['lobby'].push(data);
		}
		console.log("userInRoom.lobby: "+userInRoom['lobby']);

		for(var prop in userInRoom['lobby']){
			console.log("循环: "+userInRoom['lobby'][prop]);

		}

		var userdata = "";
		for(var prop in userInRoom['lobby']){
			console.log(userInRoom['lobby'][prop]);
			userdata =userInRoom['lobby'][prop]+", "+userdata;
		}

		io.sockets.to(socket.room).emit("user_in_room", userdata);


	});

	//接收发送的消息
	socket.on('message_to_server', function(data) {
		
		data["user"]=socket.user;
		data["room"]=socket.room;
		console.log(socket.user+" : "+"message: "+data["message"]); // log it to the Node.JS output
		io.sockets.to(socket.room).emit("message_to_client",{user:data["user"],room:data["room"],message:data["message"]});
	});

	//接收创建房间信息
	socket.on('create_public_room',function(data){
		if(rooms[data]===data){
			socket.emit("sys_post",{message:data+" is already exist."});
		}else{

			var arr = userInRoom[socket.room];
			var index = arr.indexOf(socket.user);
			console.log("user 数据类型: "+ Object.prototype.toString.apply(socket.user));
			console.log("userInRoom[socket.room]: "+ Object.prototype.toString.apply(arr));
			console.log("userInRoom index: "+index);
			userInRoom[socket.room].splice(index,1);
			

			rooms[data]=data;
			socket.leave(socket.room);
			io.sockets.to(socket.room).emit("sys_post",{message:socket.user+" left"});
			socket.join(data);
			socket.room=data;
			owner[data]=socket.user;

			var userlist = [];
			userlist.push(socket.user);
			userInRoom[data]=userlist;

			blacklist[data]=[];

			socket.emit("sys_post",{message:"Now you are in public room "+data});
		}
	});

	socket.on('join_public_room',function(data){

		if(rooms[data]===data){
			if(blacklist[data].indexOf(socket.user)!=-1){
				socket.emit("sys_post",{message:"You are not allowed to join this room."});
			}else{
				var index = userInRoom[socket.room].indexOf(socket.user);
				userInRoom[socket.room].splice(index,1);

				socket.leave(socket.room);
				io.sockets.to(socket.room).emit("sys_post",{message:socket.user+" left"});
				socket.join(data);
				socket.room=data;
				socket.emit("sys_post",{message:"Now you are in room "+data});


				userInRoom[data].push(socket.user);
				var userdata = "";
				for(var prop in userInRoom[data]){
					//console.log(userInRoom[data][prop]);
					userdata =userInRoom[data][prop]+", "+userdata;
				}

				io.sockets.to(socket.room).emit("user_in_room", userdata);
				io.sockets.to(data).emit("sys_post",{message:socket.user+" joined."});
			}
			
		}else{
			socket.emit("sys_post",{message:data+": no such room."});
		}
	});

	socket.on('create_private_room',function(data){
		if(private_rooms[data["room_name"]]!=undefined){
			socket.emit("sys_post",{message:data["room_name"]+" is already exist."});
		}else{

			var index = userInRoom[socket.room].indexOf(socket.user);
			userInRoom[socket.room].splice(index,1);

			private_rooms[data["room_name"]]=data;
			//private_rooms[data["room_name"]]["pwd"]=data["pwd"];
			console.log("私房创建成功, room_name: "+private_rooms[data["room_name"]]["room_name"]+". 密码: "+private_rooms[data["room_name"]]["pwd"]);
			socket.leave(socket.room);
			io.sockets.to(socket.room).emit("sys_post",{message:socket.user+" left"});
			socket.join(data["room_name"]);
			socket.room=data["room_name"];
			
			var userlist = [];
			userlist.push(socket.user);
			userInRoom[data["room_name"]]=userlist;

			owner[data["room_name"]]=socket.user;

			blacklist[data["room_name"]]=[];

			socket.emit("sys_post",{message:"Now you are in private room "+data["room_name"]});
		}
	});

	socket.on('join_private_room',function(data){
		if(private_rooms[data["room_name"]]!=undefined){
			if(blacklist[data["room_name"]].indexOf(socket.user)!=-1){
				socket.emit("sys_post",{message:"You are not allowed to join this room."});
			}else{
				if(private_rooms[data["room_name"]]["pwd"]===data["pwd"]){
					var index = userInRoom[socket.room].indexOf(socket.user);
					userInRoom[socket.room].splice(index,1);

					socket.leave(socket.room);
					io.sockets.to(socket.room).emit("sys_post",{message:socket.user+" left"});
					socket.join(data["room_name"]);
					socket.room=data["room_name"];
					socket.emit("sys_post",{message:"Now you are in room <"+data["room_name"]+">"});

					userInRoom[data["room_name"]].push(socket.user);

					var userdata = "";
					for(var prop in userInRoom[data["room_name"]]){
					//console.log(userInRoom[data][prop]);
					userdata =userInRoom[data["room_name"]][prop]+", "+userdata;
				}


				io.sockets.to(socket.room).emit("user_in_room", userdata);
				io.sockets.to(data["room_name"]).emit("sys_post",{message:socket.user+" joined."});
			}else{
				socket.emit("sys_post",{message:data["room_name"]+": wrong password."});
				
			}
		}

	}else {
			socket.emit("sys_post",{message:data["room_name"]+": no such room."});//房间不存在
		}

	});

	//私聊
	socket.on("message_to_user", function(data){

		var id=users[data["to_user"]];
		var msg_from = socket.user;

		console.log(socket.user+" : "+"message: "+data["message"]); // log it to the Node.JS output
		io.sockets.to(id).emit("message_to_someone",{user:msg_from,room:socket.room,message:data["message"]});
		socket.emit("sys_post",{message:"You said to "+data["to_user"]+" "+data["message"]});
	});

	//踢人
	socket.on("kick",function(data){
		if(owner[socket.room]==socket.user){
			io.sockets.to(socket.room).emit("check_kick",data);
		}else{
			socket.emit("sys_post",{message:"Only owner of this room can kick other users."});
		}
		
	});

	socket.on("complet_kick", function(data){
		console.log("user: "+socket.user+", data: "+data);
		if (data==socket.user) {

			var index = userInRoom[socket.room].indexOf(socket.user);
			userInRoom[socket.room].splice(index,1);
			var original_room = socket.room;
			socket.leave(socket.room);
			socket.join('lobby');
			socket.room='lobby';
			userInRoom['lobby'].push(data);
			socket.emit("sys_post",{message:"You were kicked out of <"+original_room+">."});

		}
	});

	socket.on("ban", function(data){
		console.log("start ban user: "+socket.user+", data: "+data);
		if(owner[socket.room]==socket.user){
			io.sockets.to(socket.room).emit("check_ban",data);
		}else{
			socket.emit("sys_post",{message:"Only owner of this room can ban other users."});
		}
	});

	socket.on("complet_ban", function(data){
		console.log("comp ban user: "+socket.user+", data: "+data);
		if (data==socket.user) {
			blacklist[socket.room].push(data);
			var index = userInRoom[socket.room].indexOf(socket.user);
			userInRoom[socket.room].splice(index,1);
			var original_room = socket.room;
			socket.leave(socket.room);
			socket.join('lobby');
			socket.room='lobby';
			userInRoom['lobby'].push(data);
			socket.emit("sys_post",{message:"You were baned from <"+original_room+">."});

		}
	});

	socket.on("unban", function(data){
		if(owner[socket.room]==socket.user){
			var index = blacklist[socket.room].indexOf(data);
			blacklist[socket.room].splice(index,1);
			io.sockets.to(data).emit("sys_post",{message:"You are unbaned from <"+socket.room+">"});
		}
	});

	
});